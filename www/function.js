function makeUser(nameValue,ageValue,addrValue,telValue,sexValue,emailValue) {
	return{
		name : nameValue,
		age : ageValue,
       addr : addrValue,
       tel : telValue,
       sex : sexValue,
       email : emailValue
	};
}

function showuser(user1){
   let detail = "";
   for (let key in user1){
    detail = detail+user1[key]+"\n";
   }
   return detail;
}
function cloneUser(user){
	let tmpUser = {};
       for(let key in user){
          tmpUser[key] = user[key];
       }
       return tmpUser;
}